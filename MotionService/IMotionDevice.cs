﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MotionService
{
    public interface IMotionDevice
    {
        public object Connect(string ip, string timeout);

        public object DisConnect(object handle);

        public object ClearError(object handle);

        public object GetProfile(object handle);

        public object SetProifile(object handle);

        public object GetSetting(object handle);

        public object SetSetting(object handle);

        public object Servo(object handle, string axis, string state);

        public object Home(object handle, string axis);

        public object Move(object handle, string axis, string target, string mode);

        public object Stop(object handle);

        //public object CheckDown();
        public object GetDI(object handle);

        public object GetDO(object handle);

        public object SetDO(object handle);
    }
}